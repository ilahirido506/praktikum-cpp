#ifndef GAJI_H
#define GAJI_H

#include <string>

void jdl_aplikasi();

void gapok_tunja(char gol, std::string status, float &gapok, float &tunja);

float prosen_potongan(float gapok);

float potongan(float gapok, float tunja, float prosen_pot);

float gaji_bersih(float gapok, float tunja, float pot);

void input(std::string &nama, char &gol, std::string &status);

void output(float gapok, float tunja, float pot, float gaber);

#endif
