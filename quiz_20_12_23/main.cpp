#include "gaji.h"

int main()
{
    system("clear");

    std::string nama = " ";
    char gol = ' ';
    std::string status = " ";

    float gapok = 0.0;
    float gaber = 0.0;
    float tunja = 0.0;
    float pot = 0.0;
    float prosen_pot = 0.05;

    jdl_aplikasi();

    input(nama, gol, status);

    gapok_tunja(gol, status, gapok, tunja);

    if (gapok > 300000)
    {
        prosen_pot = 0.1;
    }

    pot = potongan(gapok, tunja, prosen_pot);
    gaber = gaji_bersih(gapok, tunja, pot);

    output(gapok, tunja, pot, gaber);

    return 0;
}
